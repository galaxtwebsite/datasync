using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Sync.Models;

namespace Sync.Repository
{
    public class ProductRepository : IDisposable
    {
        private bool disposed = false;
        private GenericHttpClient<Product, string> productClient;
        public ProductRepository(string baseAddress, string addressSuffix)
        {
            // productClient = GenericHttpClient<Product, string>("http://localhost:17413/", "api/products/");
            productClient = new GenericHttpClient<Product, string>(baseAddress, addressSuffix);
        }
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await productClient.GetAllAsync();
        }

        public async Task<Product> GetProductAsync(string username)
        {
            return await productClient.GetAsync(username);
        }

        public async Task<Product> PostProductAsync(Product Product)
        {
            return await productClient.PostAsync(Product);
        }

        public async Task PutProductAsync(string username, Product Product)
        {
            await productClient.PutAsync(username, Product);
        }

        public async Task DeleteProductAsync(string username)
        {
            await productClient.DeleteAsync(username);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                if (productClient != null)
                {
                    var mc = productClient;
                    productClient = null;
                    mc.Dispose();
                }
                disposed = true;
            }
        }
    }
}