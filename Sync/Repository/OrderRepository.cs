using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Sync.Models;

namespace Sync.Repository
{
    public class OrderRepository : IDisposable
    {
        private bool disposed = false;
        private GenericHttpClient<Order, string> orderClient;

        public OrderRepository(string baseAddress, string addressSuffix)
        {
            // orderClient = new GenericHttpClient<Order, string>("http://localhost:17413/", "api/orders/");
            orderClient = new GenericHttpClient<Order, string>(baseAddress, addressSuffix);
        }
        public async Task<IEnumerable<Order>> GetOrdersAsync()
        {
            return await orderClient.GetAllAsync();
        }

        public async Task<Order> GetOrderAsync(string username)
        {
            return await orderClient.GetAsync(username);
        }

        public async Task<Order> PostOrderAsync(Order Order)
        {
            return await orderClient.PostAsync(Order);
        }

        public async Task PutOrderAsync(string username, Order Order)
        {
            await orderClient.PutAsync(username, Order);
        }

        public async Task DeleteOrderAsync(string username)
        {
            await orderClient.DeleteAsync(username);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                if (orderClient != null)
                {
                    var mc = orderClient;
                    orderClient = null;
                    mc.Dispose();
                }
                disposed = true;
            }
        }
    }
}

