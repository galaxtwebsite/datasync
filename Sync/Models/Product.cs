using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Sync.Models
{
    public class Variant
    {
        public object id { get; set; }
        public object product_id { get; set; }
        public string title { get; set; }
        public string price { get; set; }
        public string sku { get; set; }
        public string compare_at_price { get; set; }
        public string option1 { get; set; }
        public object option2 { get; set; }
        public object option3 { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int inventory_quantity { get; set; }
        public int old_inventory_quantity { get; set; }
    }

    public class Product
    {
        public long id { get; set; }
        public string title { get; set; }
        public string vendor { get; set; }
        public string product_type { get; set; }
        public IList<Variant> variants { get; set; }
    }
}