﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Sync
{
    public class GenericHttpClient<T, TResourceIdentifier> : IDisposable where T : class
    {

        private bool disposed = false;
        private HttpClient httpClient;
        protected readonly string serviceBaseAddress;
        private readonly string addressSuffix;
        private readonly string jsonMediaType = "application/json";

        public GenericHttpClient(string serviceBaseAddress, string addressSuffix)
        {
            this.serviceBaseAddress = serviceBaseAddress ?? throw new ArgumentNullException(paramName: nameof(serviceBaseAddress));
            this.addressSuffix = addressSuffix ?? throw new ArgumentNullException(paramName: nameof(addressSuffix));
            httpClient = BuildHttpClient(serviceBaseAddress);
        }

        protected virtual HttpClient BuildHttpClient(string serviceBaseAddress)
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(serviceBaseAddress);
            httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(jsonMediaType));
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add(StringWithQualityHeaderValue.Parse("gzip"));
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add(StringWithQualityHeaderValue.Parse("default"));
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("Matlus_HttpClient", "1.0")));
            return httpClient;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var responseMessage = await httpClient.GetAsync(addressSuffix);
            responseMessage.EnsureSuccessStatusCode();
            return await responseMessage.Content.ReadAsAsync<IEnumerable<T>>();
        }

        public async Task<T> GetAsync(TResourceIdentifier identifier)
        {
            var responseMessage = await httpClient.GetAsync(addressSuffix + identifier.ToString());
            responseMessage.EnsureSuccessStatusCode();
            return await responseMessage.Content.ReadAsAsync<T>();
        }

        public async Task<T> PostAsync(T model)
        {
            // var requestMessage = new HttpRequestMessage();
            var objectContent = CreateJsonObjectContent(model);
            var responseMessage = await httpClient.PostAsync(addressSuffix, objectContent);
            return await responseMessage.Content.ReadAsAsync<T>();
        }
        public async Task PutAsync(TResourceIdentifier identifier, T model)
        {
            // var requestMessage = new HttpRequestMessage();
            var objectContent = CreateJsonObjectContent(model);
            await httpClient.PutAsync(addressSuffix + identifier.ToString(), objectContent);
        }

        public async Task DeleteAsync(TResourceIdentifier identifier)
        {
            await httpClient.DeleteAsync(addressSuffix + identifier.ToString());
        }
        private ObjectContent CreateJsonObjectContent(T model)
        {
            return new ObjectContent(typeof(T), model, new JsonMediaTypeFormatter());
        }
        #region IDisposable Members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                if (httpClient != null)
                {
                    var hc = httpClient;
                    httpClient = null;
                    hc.Dispose();
                }
                disposed = true;
            }
        }
        #endregion
    }
}
